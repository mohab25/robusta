//
//  UITableViewExtension.swift
//  iOSRobustaTask
//
//  Created by Abulrahman mohsen on 22/11/2022.
//

import UIKit

extension UITableView {
    func dequeueCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
         guard let cell = self.dequeueReusableCell(withIdentifier: T.className, for: indexPath) as? T else {
             fatalError("DequeueReusableCell failed while casting") }
        return cell
    }
    func register<T: UITableViewCell>(cell: T.Type) {
        register(cell.nib, forCellReuseIdentifier: cell.className)
    }
}
