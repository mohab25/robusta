//
//  UIViewExtension.swift
//  iOSRobustaTask
//
//  Created by Abulrahman mohsen on 22/11/2022.
//

import UIKit

extension UIView {
    static var nib: UINib {
         UINib(nibName: self.className, bundle: nil)
    }
}
