//
//  NSObjectExtension.swift
//  iOSRobustaTask
//
//  Created by Abulrahman mohsen on 22/11/2022.
//

import Foundation

extension NSObject {
    class var className: String { "\(self)" }
}
