//
//  UITableViewCellExtensions.swift
//  iOSRobustaTask
//
//  Created by Abulrahman mohsen on 22/11/2022.
//

import UIKit

extension UITableViewCell {
    class var cellId: String { "\(self)" }
}
