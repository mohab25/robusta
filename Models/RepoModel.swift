//
//  RepoModel.swift
//  iOSRobustaTask
//
//  Created by Abulrahman mohsen on 22/11/2022.
//

import Foundation

struct RepoModel: Codable {
    let id: Int
    let name: String
    let isPrivate: Bool
    let owner: Owner
    let description: String?
    let fork: Bool
    init(
        id: Int,
        name: String,
        isPrivate: Bool,
        owner: Owner,
        description: String?,
        fork: Bool
    ) {
        self.id = id
        self.name = name
        self.isPrivate = isPrivate
        self.owner = owner
        self.description = description
        self.fork = fork
    }
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case isPrivate = "private"
        case owner
        case description
        case fork
    }
}

// MARK: - Owner
struct Owner: Codable {
    let userName: String
    let avatarURL: String
    let type: RepoType
    enum CodingKeys: String, CodingKey {
        case userName = "login"
        case avatarURL = "avatar_url"
        case type
    }
}

enum RepoType: String, Codable {
    case organization = "Organization"
    case user = "User"
}
