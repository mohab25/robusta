//
//  RemoteImageView.swift
//  iOSRobustaTask
//
//  Created by Abulrahman mohsen on 22/11/2022.
//

import UIKit

class RemoteImageView: UIImageView {
    var imageUrlString: String?
    static var imageCache = NSCache<NSString, UIImage>()

    func loadImage(urlString: String) {
        imageUrlString = urlString
        guard let url = URL(string: urlString) else { return }
        image = nil
        if let imageFromCache = RemoteImageView.imageCache.object(forKey: urlString as NSString) {
            self.image = imageFromCache
            return
        }
        URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) in
            if error != nil {
                print(error ?? "")
                return
            }
            DispatchQueue.main.async {
                guard let imageToCache = UIImage(data: data!) else { return }
                if self.imageUrlString == urlString {
                    self.image = imageToCache
                }
                RemoteImageView.imageCache.setObject(imageToCache, forKey: urlString as NSString)
            }
        }).resume()
    }
}
