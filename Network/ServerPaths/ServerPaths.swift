//
//  ServerPaths.swift
//  iOSRobustaTask
//
//  Created by Abulrahman mohsen on 22/11/2022.
//

import Foundation

public enum ServerPaths: String {
    case repositories
    private var baseURL: String { "https://api.github.com/" }
    var value: String { baseURL + rawValue }
}
