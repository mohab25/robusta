//
//  RepositoryDetailsViewController.swift
//  RobustaTask
//
//  Created by mohab on 02/04/2023.
//

import UIKit

final class RepositoryDetailsViewController: UIViewController {
    // MARK: - outlets
    @IBOutlet private weak var detailsLabel: UILabel!
    @IBOutlet private weak var privateLabel: UILabel!
    @IBOutlet private weak var repoNameLabel: UILabel!
    @IBOutlet private weak var userDetailsLabel: UILabel!
    @IBOutlet private  weak var userImage: RemoteImageView!
    @IBOutlet private weak var UserNameLabel: UILabel!
    // MARK: - variables
    var presenter: RepoDetailsPresenterProtocol!
    override func viewDidLoad() {
        super.viewDidLoad()
        userImage.layer.cornerRadius = userImage.frame.height / 2
        presenter.viewDidload()
    }
    
}
// MARK: - RepoDetailsPresentation
extension RepositoryDetailsViewController: RepoDetailsPresentation {
    func configureView(with item: RepoItem) {
        userImage.loadImage(urlString: item.owner.avatarURL)
        UserNameLabel.text = item.owner.userName
        userDetailsLabel.text = item.owner.type.rawValue
        repoNameLabel.text = item.name
        privateLabel.text = item.isPrivate.description
        detailsLabel.text = item.description
    }
}
