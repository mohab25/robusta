//
//  RepositoryDetailsPresenter.swift
//  RobustaTask
//
//  Created by mohab on 02/04/2023.
//

import Foundation

protocol RepoDetailsPresentation: AnyObject {
    func configureView(with item: RepoItem)
}

protocol RepoDetailsPresenterProtocol {
    func viewDidload()
}

final class RepoDetailsPresenter: RepoDetailsPresenterProtocol {
    private weak var view: RepoDetailsPresentation!
    private var item: RepoItem!
    init(
        view: RepoDetailsPresentation,
        item: RepoItem
    ) {
        self.view = view
        self.item = item
    }
    func viewDidload() {
        view.configureView(with: item)
    }
}
