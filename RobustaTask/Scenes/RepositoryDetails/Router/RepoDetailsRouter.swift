//
//  RepoDetailsRouter.swift
//  RobustaTask
//
//  Created by mohab on 02/04/2023.
//

import UIKit

class RepoDetailsRouter {
    static func createRepoDetails(item: RepoItem) -> UIViewController {
        let controller = RepositoryDetailsViewController()
        let presenter = RepoDetailsPresenter(view: controller, item: item)
        controller.presenter = presenter
        return controller
    }
}
