//
//  RepoListRouter.swift
//  RobustaTask
//
//  Created by mohab on 02/04/2023.
//

import UIKit
protocol RepoListRouterProtocol: AnyObject {
    func showRepoDetails(item: RepoItem)
}

final class RepoListRouter {
    private weak var viewController: RepositoryViewController?
    init(viewController: RepositoryViewController) {
        self.viewController = viewController
    }
    static func createRepoList() -> UIViewController {
        let controller = RepositoryViewController()
        let interactor = RepoListInteractor()
        let router = RepoListRouter(viewController: controller)
        let presenter = RepoListPresenter(view: controller, interactor: interactor, router: router)
        controller.presenter = presenter
        return controller
    }
}

extension RepoListRouter: RepoListRouterProtocol {
    func showRepoDetails(item: RepoItem) {
        let repoDetailsViewController = RepoDetailsRouter.createRepoDetails(item: item)
        viewController?.show(repoDetailsViewController, sender: nil)
    }
}
