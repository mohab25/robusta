//
//  RepositoryCell.swift
//  RobustaTask
//
//  Created by mohab on 02/04/2023.
//

import UIKit
protocol RepoListCellPresentable {
    func configureRepoCell(item: RepoItem)
}
class RepositoryCell: UITableViewCell {
    @IBOutlet private weak var repoUserImage: RemoteImageView!
    @IBOutlet private weak var repoNameLabel: UILabel!
    @IBOutlet private weak var repoUserNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        repoUserImage.layer.cornerRadius = repoUserImage.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
    }
    
    func configureRepoCell(item: RepoItem){
        repoUserNameLabel.text = item.owner.userName
        repoNameLabel.text = item.name
        repoUserImage.loadImage(urlString: item.owner.avatarURL)
    }
}
extension RepositoryCell: RepoListCellPresentable {
    func configure(item: RepoItem) {
        repoUserNameLabel.text = item.owner.userName
        repoNameLabel.text = item.name
        repoUserImage.loadImage(urlString: item.owner.avatarURL)
    }
}
