//
//  RepositoryViewController.swift
//  RobustaTask
//
//  Created by mohab on 02/04/2023.
//

import UIKit

final class RepositoryViewController: UIViewController {
    // MARK: - outlets
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var tableView: UITableView!
    // MARK: - variables
    var presenter: RepoListPresenterProtocol!
    var refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        searchBar.delegate = self
        presenter.fetchData()
        setupRefreshController()
    }
    private func setupTableView() {
        tableView.register(cell: RepositoryCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    

    private func setupRefreshController() {
        refreshControl.attributedTitle = .init(string: "restore data")
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(refreshAction), for: UIControl.Event.valueChanged)
        refreshControl.transform = .init(scaleX: 0.8, y: 0.8)
        tableView.refreshControl = refreshControl
    }
    @objc private func refreshAction() {
        searchBar.text?.removeAll()
        presenter.resetSearchResult()
        refreshControl.endRefreshing()
    }
}
// MARK: - UISearchBarDelegate
extension RepositoryViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        guard let search = searchBar.text, search.count > 1 else { return }
        presenter.search(query: search)
    }
}
// MARK: - RepoListPresentation
extension RepositoryViewController: RepoListPresentation {
    func showLoader() {
        // Display UI Loader To Enhance UX
    }
    func dismissLoader() {
        // Display UI Loader To Enhance UX
    }
    func showError(error: String) {
        print(error)
    }
    func reloadData() {
        tableView.reloadData()
    }
}
// MARK: - UITableViewDelegate, UITableViewDataSource
extension RepositoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RepositoryCell = tableView.dequeueCell(for: indexPath)
        presenter.configure(cell: cell, for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRowAt(indexPath: indexPath)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
