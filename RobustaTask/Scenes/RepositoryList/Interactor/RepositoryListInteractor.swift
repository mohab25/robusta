//
//  RepositoryListInteractor.swift
//  RobustaTask
//
//  Created by mohab on 02/04/2023.
//



import UIKit

typealias RepoListResult = (Result<[RepoItem], Error>)

protocol RepoListInteractorProtocol {
    func getRepoList(handler: @escaping (RepoListResult) -> Void )
}

final class RepoListInteractor: RepoListInteractorProtocol {
    func getRepoList(handler: @escaping (RepoListResult) -> Void) {
        NetworkManager.request(model: [RepoModel].self, url: .repositories) { result in
            let handlerRepos = result.map({repos in
                repos.map(RepoItem.init)
            })
            handler(handlerRepos)
        }
    }
}
