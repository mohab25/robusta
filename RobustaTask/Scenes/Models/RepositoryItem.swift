//
//  RepositoryItem.swift
//  RobustaTask
//
//  Created by mohab on 02/04/2023.
//

import Foundation

struct RepoItem: Equatable {
    let id: Int
    let name: String
    let isPrivate: Bool
    let owner: OwnerItem
    let description: String
    let fork: Bool

    static func == (lhs: RepoItem, rhs: RepoItem) -> Bool { lhs.id == rhs.id }
}

extension RepoItem {
    init(model: RepoModel) {
        id = model.id
        name = model.name
        isPrivate = model.isPrivate
        owner = OwnerItem(model: model.owner)
        description = model.description ?? ""
        fork = model.fork
    }
}
struct OwnerItem {
    let userName: String
    let avatarURL: String
    let type: RepoType
}

extension OwnerItem {
    init(model: Owner) {
        userName = model.userName
        avatarURL = model.avatarURL
        type = model.type
    }
}
